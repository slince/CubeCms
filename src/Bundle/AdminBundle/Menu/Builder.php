<?php

namespace Cube\Bundle\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;

class Builder
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->addChild('User');
        $menu['User']->addChild('User List', [
            'route' => 'admin_user_list'
        ]);
    }
}
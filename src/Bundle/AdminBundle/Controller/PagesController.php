<?php

namespace Cube\Bundle\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class PagesController extends Controller
{
    /**
     * 后台首页
     *
     * @Route("/", name="admin_dashboard")
     */
    public function index()
    {
        return $this->render('CubeAdminBundle:pages:index.html.twig');
    }
}
<?php

namespace Cube\Bundle\AdminBundle\Controller;

use Cube\Component\User\UserManagerInterface;
use Doctrine\Common\Collections\Criteria;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * @var PaginatorInterface
     */
    protected $paginator;

    public function __construct(UserManagerInterface $userManager, PaginatorInterface $paginator)
    {
        $this->userManager = $userManager;
        $this->paginator = $paginator;
    }

    /**
     * 用户列表
     *
     * @Route("/users", name="admin_user_list")
     * @param Request $request
     * @return Response
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function index(Request $request)
    {
        $criteria = Criteria::create();
        $query = $this->userManager->findUserQuery($criteria);
        $users = $this->paginator->paginate(
            $query,
            $request->query->getInt('page', 1)/*page number*/,
            10
        );
        return $this->render('CubeAdminBundle:user:index.html.twig', [
            'user' => $users
        ]);
    }
}
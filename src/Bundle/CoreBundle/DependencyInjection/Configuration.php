<?php

namespace Cube\Bundle\CoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cube');

        $rootNode
            ->fixXmlConfig('entity_class')
            ->children()
                ->arrayNode('entity_classes')
                    ->useAttributeAsKey('name')
                    ->scalarPrototype() ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
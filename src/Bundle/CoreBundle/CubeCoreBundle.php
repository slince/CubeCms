<?php

namespace Cube\Bundle\CoreBundle;

use Cube\Bundle\CoreBundle\DependencyInjection\CubeCoreExtension;
use Cube\Component\Base\ServiceManagerInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CubeCoreBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        $container->registerForAutoconfiguration(ServiceManagerInterface::class)
            ->addTag('cube.manager');
    }

    /**
     * {@inheritdoc}
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new CubeCoreExtension();
        }
        return $this->extension;
    }
}
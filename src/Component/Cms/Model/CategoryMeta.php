<?php

/*
 * This file is part of the slince/pandacms
 *
 * (c) Slince <taosikai@yeah.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Cube\Component\Cms\Model;

use Cube\Component\Base\Model\AbstractMeta;
use Cube\Component\Base\Model\IdentifiableTrait;

class CategoryMeta extends AbstractMeta implements CategoryMetaInterface
{
    use IdentifiableTrait;

    /**
     * @var CategoryInterface
     */
    protected $category;

    /**
     * @return CategoryInterface
     */
    public function getCategory(): CategoryInterface
    {
        return $this->category;
    }

    /**
     * @param CategoryInterface $category
     * @return CategoryMeta
     */
    public function setCategory(CategoryInterface $category): CategoryMeta
    {
        $this->category = $category;

        return $this;
    }
}
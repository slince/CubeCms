<?php

/*
 * This file is part of the slince/pandacms
 *
 * (c) Slince <taosikai@yeah.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Cube\Component\Cms\Model;

use Cube\Component\Tag\Model\TagsAwareTrait;
use Cube\Component\Base\Model\CommentableTrait;
use Cube\Component\Base\Model\ContentTrait;
use Cube\Component\Base\Model\DateTimeTrait;
use Cube\Component\Base\Model\EnabledTrait;
use Cube\Component\Base\Model\IdentifiableTrait;
use Cube\Component\Base\Model\MetasAwareTrait;
use Cube\Component\User\Model\UserAwareTrait;
use Cube\Component\User\Model\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Post implements PostInterface
{
    use IdentifiableTrait, ContentTrait,
        DateTimeTrait, EnabledTrait,
        TagsAwareTrait, UserAwareTrait,
        MetasAwareTrait, CommentableTrait;

    /**
     * 类型
     * @var string
     */
    const TYPE_ARTICLE = 'article';

    /**
     * 类型
     * @var string
     */
    protected $type;

    /**
     * @var string
     */
    protected $title;

    /**
     * 阅读数量
     * @var int
     */
    protected $viewCount;

    /**
     * @var CategoryInterface[]|Collection
     */
    protected $categories;

    public function __construct(?string $type = null)
    {
        $this->metas = new ArrayCollection();
        $this->categories = new ArrayCollection();
        $this->type = $type ?? static::TYPE_ARTICLE;
    }


    /**
     * {@inheritdoc}
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * {@inheritdoc}
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function setType(string $type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthor(): UserInterface
    {
        return $this->getUser();
    }

    /**
     * @return int
     */
    public function getViewCount(): int
    {
        return $this->viewCount;
    }

    /**
     * {@inheritdoc}
     */
    public function setViewCount(int $viewCount): Post
    {
        $this->viewCount = $viewCount;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addViewCount(int $count = 1): Post
    {
        $this->viewCount += $count;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getCategories(): array
    {
        return $this->categories;
    }

    /**
     * {@inheritdoc}
     */
    public function addCategory(CategoryInterface $category)
    {
        $this->categories = $category;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function removeCategory(CategoryInterface $category)
    {
        $this->categories->removeElement($category);
        return $this;
    }
}
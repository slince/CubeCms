<?php

/*
 * This file is part of the slince/pandacms
 *
 * (c) Slince <taosikai@yeah.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

declare(strict_types=1);

namespace Cube\Component\Cms\Model;

use Cube\Component\Base\Model\AbstractComment;
use Cube\Component\Base\Model\IdentifiableTrait;
use Cube\Component\Base\Model\MetasAwareTrait;
use Cube\Component\User\Model\UserAwareTrait;

class Comment extends AbstractComment implements CommentInterface
{
    use IdentifiableTrait, MetasAwareTrait, UserAwareTrait;

    /**
     * @var PostInterface
     */
    protected $post;

    /**
     * {@inheritdoc}
     */
    public function getPost(): PostInterface
    {
        return $this->post;
    }

    /**
     * {@inheritdoc}
     */
    public function setPost(PostInterface $post): Comment
    {
        $this->post = $post;

        return $this;
    }
}
<?php

/*
 * This file is part of the slince/pandacms
 *
 * (c) Slince <taosikai@yeah.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cube\Component\Cms\Model;

use Cube\Component\Base\Model\CommentableInterface;
use Cube\Component\Base\Model\CommentInterface;
use Cube\Component\Base\Model\ContentInterface;
use Cube\Component\Base\Model\DateTimeInterface;
use Cube\Component\Base\Model\EnabledInterface;
use Cube\Component\Base\Model\IdentifiableInterface;
use Cube\Component\Base\Model\MetasAwareInterface;
use Cube\Component\Tag\Model\TagsAwareInterface;
use Cube\Component\User\Model\UserAwareInterface;
use Cube\Component\User\Model\UserInterface;

interface PostInterface extends
    IdentifiableInterface,
    ContentInterface,
    UserAwareInterface,
    TagsAwareInterface,
    MetasAwareInterface,
    CommentableInterface,
    EnabledInterface,
    DateTimeInterface
{
    /**
     * 获取类型
     *
     * @return string
     */
    public function getType(): string;

    /**
     * 获取标题
     * @return string
     */
    public function getTitle(): string ;

    /**
     * 获取阅读数量
     * @return int
     */
    public function getViewCount(): int;

    /**
     * 设置阅读数量
     * @param int $count
     * @return self
     */
    public function setViewCount(int $count);

    /**
     * 增加阅读数量
     * @param int $count
     * @return self
     */
    public function addViewCount(int $count = 1);

    /**
     * 获取作者
     * @return UserInterface
     */
    public function getAuthor();

    /**
     * 获取meta
     * @return PostMetaInterface[]
     */
    public function getMetas(): array;

    /**
     * 获取所有分类
     * @return CategoryInterface[]
     */
    public function getCategories(): array;

    /**
     * 添加分类
     * @param CategoryInterface $category
     * @return self
     */
    public function addCategory(CategoryInterface $category);

    /**
     * 移除分类
     * @param CategoryInterface $category
     * @return self
     */
    public function removeCategory(CategoryInterface $category);
}
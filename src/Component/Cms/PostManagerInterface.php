<?php

namespace Cube\Component\Cms;

use Cube\Component\Base\ServiceManagerInterface;
use Cube\Component\Cms\Model\PostInterface;

interface PostManagerInterface extends ServiceManagerInterface
{
    /**
     * 创建空白文章
     *
     * @return PostInterface
     */
    public function createBlankPost();

    /**
     * 保存文章
     *
     * @param PostInterface $post
     */
    public function savePost(PostInterface $post);
}
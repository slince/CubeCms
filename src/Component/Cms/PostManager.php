<?php

namespace Cube\Component\Cms;

use Cube\Component\Base\ServiceManager;
use Cube\Component\Cms\Model\PostInterface;
use Doctrine\ORM\EntityManagerInterface;

class PostManager extends ServiceManager implements PostManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * {@inheritdoc}
     */
    public function createBlankPost()
    {
        $class = $this->getEntityClass();
        return new $class();
    }

    /**
     * {@inheritdoc}
     */
    public function savePost(PostInterface $post)
    {
        $this->entityManager->persist($post);
        $this->entityManager->flush();
    }
}
<?php

/*
 * This file is part of the slince/pandacms
 *
 * (c) Slince <taosikai@yeah.net>
 *
 * This source file is subject to the MIT license that is bundled
 * with this source code in the file LICENSE.
 */

namespace Cube\Component\Tag\Model;


use Cube\Component\Base\Model\DateTimeTrait;
use Cube\Component\Base\Model\IdentifiableTrait;

class Tag implements TagInterface
{
    use IdentifiableTrait, DateTimeTrait;

    /**
     * @var string
     */
    protected $name;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Tag
     */
    public function setName(string $name): Tag
    {
        $this->name = $name;
        return $this;
    }
}
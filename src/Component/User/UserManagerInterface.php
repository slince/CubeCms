<?php

namespace Cube\Component\User;

use Cube\Component\Base\ServiceManagerInterface;
use Doctrine\Common\Collections\Criteria;
use FOS\UserBundle\Model\UserManagerInterface as FOSUserManagerInterface;

interface UserManagerInterface extends ServiceManagerInterface, FOSUserManagerInterface
{
    /**
     * 查找用户
     *
     * @param Criteria $criteria
     * @return \Doctrine\ORM\Query
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function findUserQuery(Criteria $criteria);
}
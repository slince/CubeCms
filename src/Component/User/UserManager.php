<?php

namespace Cube\Component\User;

use Doctrine\Common\Collections\Criteria;
use FOS\UserBundle\Doctrine\UserManager as FOSUserManager;

class UserManager extends FOSUserManager implements UserManagerInterface
{
    /**
     * {@inheritdoc}
     */
    public function setEntityClass($class)
    {
        //ignore
    }

    /**
     * {@inheritdoc}
     */
    public function getEntityClass()
    {
        return $this->getClass();
    }

    /**
     * {@inheritdoc}
     */
    public function findUserQuery(Criteria $criteria)
    {
        return $this->getRepository()->createQueryBuilder('u')
            ->addCriteria($criteria)
            ->getQuery();
    }
}
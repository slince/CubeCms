<?php

namespace Cube\Component\Base;

class ServiceManager
{
    protected $entityClass;

    public function setEntityClass($class)
    {
        $this->entityClass = $class;
    }

    public function getEntityClass()
    {
        return $this->entityClass;
    }
}
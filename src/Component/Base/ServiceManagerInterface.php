<?php

namespace Cube\Component\Base;

interface ServiceManagerInterface
{
    /**
     * @return string
     */
    public function getEntityClass();

    /**
     * @param string $class
     */
    public function setEntityClass($class);
}
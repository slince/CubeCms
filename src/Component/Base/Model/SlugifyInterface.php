<?php

namespace Cube\Component\Base\Model;

interface SlugifyInterface
{
    /**
     * 获取slug.
     *
     * @return string
     */
    public function getSlug();
}

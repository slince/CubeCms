<?php

namespace Cube\Component\Base\Model;

use Cube\Component\User\Model\UserAwareTrait;

abstract class AbstractComment implements CommentInterface
{
    use ContentTrait, DateTimeTrait, UserAwareTrait, EnabledTrait;
}

<?php
/**
 * Panda vote component.
 *
 * @author Tao <taosikai@yeah.net>
 */

namespace  Cube\Component\Base\Model;

interface VoteInterface extends DateTimeInterface, IdentifiableInterface
{
    /**
     * 获取id.
     *
     * @return int
     */
    public function getId();
}

<?php

namespace Cube\Component\Base\Model;

interface IdentifiableInterface
{
    /**
     * 获取id.
     *
     * @return int
     */
    public function getId();
}

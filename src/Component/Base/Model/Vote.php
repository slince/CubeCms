<?php
/**
 * Panda Core Component.
 *
 * @author Tao <taosikai@yeah.net>
 */

namespace Cube\Component\Base\Model;

use Cube\Component\User\Model\UserAwareTrait;

abstract class Vote implements VoteInterface
{
    use DateTimeTrait, UserAwareTrait, IdentifiableTrait;
}
